# Meeting Minutes

## August 18th 2020 5PM PST

Participants: Ben Coombs, Dr Erich Schulz, Robert L. Reid, PhD, Brittany Tran, Pawan Murthy, Steven Carr, Daren Schwenke

Duration: 1.5hours

## Notes:

- Intros
- Pawan Murthy (project/product management)
- Steven Carr, Daren Schwenke
  - COSV (https://github.com/Arcus-3d/cosv)
  - Arduino Teensy
- Brittany Tran
  - Contact with COSV, OpenVentBristol, in Slack channel with RespiraWorks
- Project Management
  - Formalize schedules, sprints etc.
- Volunteers
  - Listen to what people are willing to give us and make the best use of volunteers time
- GitLab Issue Queue
  - Top priority story to complete: #20 & #2 - Overpressure alarm
    - “Hello world” feature
    - Create an alarm when the pressure is too high
  - #22 Merge Rules
    - Ben & Erich approve merges to master
    - Make “dev” branch the default branch anyone can commit to
    - Also have “test” branch before “master”
    - Revise if workload becomes too onerous
  - #20 Risk analysis overpressure alarm
    - Software doesn’t detect overpressure
    - Transient (eg. < 10ms) that the software throws an alarm on this sensor error
    - Alarm doesn’t sound on a subsequent overpressure event while the alarm is silenced
    - User turns off alarms because it’s annoying
    - Patient coughs - is this an overpressure alarm?
    - Build synthetic trace and unit test
- Engineering Discussion
  - Sample time - 100Hz is an ideal goal, reality is less, eg. 20-50Hz
    - Capture this in the risk analysis: certain hardware would be too slow to capture an over pressure alarm - row 23 (need to number these)
    - Hardware RA
      - Sensors and valves have variable sample rate and resolution which can have adverse effects.
    - Alarms
      - Graduated assertiveness
      - Standard alarms IEC 60601
      - User needs to silence alarm to think - information overload
      - Future work: bag breaks - flow is lower than expected
- General Discussion
    - Are we in the EUA or the FDA arena?
    - Split backend (control) from frontend (UI)
