# HE-xxx VentOS
## Intended Use

The intended use of this software is to act as a component in a well engineered
and approved ventilator alarm system that has, at a minimum, two pressure
sensors and two flow sensors and has been subject to rigorous integration
testing, in a properly staffed hospital environment with 1:1 nurse to patient
ratio and continuous co-monitoring of patient status using both pulse oximetry
and capnography alarm systems.

Reasonably foreseeable misuse of this software, for the duration of the
pandemic, and on an ongoing basis in overwhelmed health care systems in poorly
resourced countries, may include using this device to run as the sole patient
monitor in hastily constructed hardware being used as a last-resort option to
prevent inevitable death.

Note that under intended use, this is an IEC:63204 class A software system,
 but under reasonably foreseeable misuse this is a class C software system.

fixme: the above is specific to an alarm system
