# HE-xxx VentOS
## Design Input Specification

### 1. Use
The approval and release of this document shall follow HE-QS-SOP004.

### 2. Purpose and Scope
This Design Input Specification aims to meet the requirements of the
HE-QS-QualityManual section:

7.3.3 Design and development inputs

Design inputs, including functional, performance, usability, safety, standards,
and applicable statutory and regulatory requirements, are identified and
documented in the Design Input Specification. Outputs of risk analysis are
also included.

The design inputs are evaluated, reviewed and approved to ensure that they
are complete, consistent and unambiguous, able to be verified or validated,
and that they address the intended use of the product.

HE-QS-SOP003-DesignControls

### 3. Design Input Specification
#### 3.1 Functional
1. Ventilator layout
1. Minimum requirements
1. Ventilator modes
1. Data storage formats
1. Interfaces
1. Data exchange formats
1. RTOS?
1. TDD requirements
#### 3.2 Performance
1. Pressure sensor read frequency
    1. Min 20Hz
    1. Ideal 50Hz
    1. Max 100Hz
1. Flow sensor read frequency
    1. Min 20Hz
    1. Ideal 50Hz
    1. Max 100Hz
1. Oxygen sensor read frequency
    1. Min 1Hz
    1. Ideal 5Hz
    1. Max 10Hz
1. CO2 sensor read frequency
    1. Min 1Hz
    1. Ideal 5Hz
    1. Max 10Hz
1. Pulse oximeter sensor read frequency
    1. Min 1Hz
    1. Ideal 5Hz
    1. Max 10Hz
1. Solenoid on/off activation frequency
    1. Min 1Hz
    1. Ideal 2Hz
    1. Max 4Hz
1. Solenoid proportional PWM frequency
    1. Min 200Hz
    1. Ideal 500Hz
    1. Max 1000Hz
1. Motor PWM frequency
    1. Min 200Hz
    1. Ideal 500Hz
    1. Max 1000Hz
1. Digital display refresh rate
    1. Min 15Hz
    1. Ideal 30Hz
    1. Max 60Hz
1. Touch screen response rate
    1. Min 1s
    1. Ideal 0.5s
    1. Max 0.25s
1. Alarm sound
    1. 80dB at 1m
    1. Standards
1. Alarm lighting
    1. LED
    1. Green, yellow, red
    1. Visibility arc (360?)
    1. Brightness xx Lumens
    1. Standards
#### 3.3 Usability
1. UI requirements
1. Touch screen
1. Language
1. Graphics/text
#### 3.4 Safety
1. Alarms
#### 3.5 Standards
- IEC 62304 - Medical device software - software lifecycle processes.  
- ISO 14971 - Application of risk management to medical devices. 
- ISO 13485 - Quality management systems - requirements for regulatory purposes. 
- IEC 60601 - Medical electrical equipment.  
- IEC 62366 - Application of usability engineering to medical devices. 


Also see https://www.iso.org/ics/11.040.10/x/ for all ISO standards relevant to anaesthetic and respiratory equipment

#### 3.6 Statutory
1. Laws
#### 3.7 Regulatory
1. Regs
### 4. Risk Analysis Outputs

Risk matrix

