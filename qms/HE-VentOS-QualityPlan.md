# HE-xxx VentOS
## Quality Plan

### 1. Use
The Title above shall be replaced by the project name and call-sign (xxx fixme).
The document shall be named HE-xxx-QualityPlan with xxx being the actual call-sign.
The approval and release of this document shall then follow HE-QS-SOP004

### 2. Purpose and Scope
This document shall be filled out following HE-QS-SOP002 for the specifics of each project.

### 3. Reference Documents
To be completed. todo

### 4. Background and scope of the project

#### 4.1 History
Hundreds of open source ventilators have been developed over the first 6 months
of the COVID-19 Pandemic. In the interests of time, few projects have followed
a systematic and comprehensive QA/RA process required for medical devices.
Global supply restrictions have created demand for locally manufactured
ventilators. Numerous mechanical designs have emerged, however, these
ventilators are all limited by the software.

#### 4.1 Scope
VentOS is a software only project. VentOS aims to produce an embedded
ventilator software framework that can be used on multiple hardware platforms
in conjunction with platform-dependent drivers.
The VentOS team will liaise with existing ventilator teams to establish user
requirements such as hardware platforms, but the design and build of a
complete ventilator is outside of the scope of this project.

### 5. Product Quality Objectives and Requirements
#### 5.1 Product Life
The product life of VentOS is expected to be 10 years. VentOS is open
source embedded software that can be maintained for many years by the community
or ADOPTING PARTNERS.

#### 5.2 Delivery target - manufacturing partnership

VentOS aims to partner with existing open source teams developing ventilator
hardware, and potentially with commercial ventilator manufacturers in the future.

#### 5.3 Regulatory Profile
To be determined. (todo)
### 6. Schedule

VentOS will follow an iterative agile process. Our plan is to build a framework
on mock sensors and mock components first, and then attempt to integrate with
a team that has fully opened their code, such as OpenVent Bristol. Thereafter
the utility of VentOS will be ehanced in several dimensions:
1. suitability to more hardware,
1. expansion of ventilator modes, and
1. expansion of safety features, such as continual addition of software alarm conditions.

### 7. Resources
#### 7.1 Infrastructure
VentOS uses PlatformIO (C++) and Python for software development.
All VentOS source code and documentation is shared online in the public GitLab
repository.
Documentation is recorded in Markdown to leverage GitLab source control.
Excel is used when Markdown is not suitable, such as the FMEA matrix.
Images are used for schematics and must be versioned.
ZenQMS is used for digital document signing.
Software is developed by volunteer engineers with a ‘bring your own device’
computer policy.
Communication is conducted on Slack and Google Meet.

#### 7.2 Human Resources
- Dr. Erich Schulz, Anesthetist, Project Lead
- Robert Read, PhD Computer Science, Project Lead
- Ben Coombs, ME, Lead Engineer
- Outreach Co-ordinator - Brittany Tran
- Helpful Engineering - expert consultants such as QA/RA, project managers, PR
- Software Developers from partnership projects

### 8. Risk Management and Usability
#### 8.1 Risk management

##### 8.1.1 Relevant Standards
IEC 62304 - Medical device software - software lifecycle processes
ISO 14971 - Application of risk management to medical devices
ISO 13485 - Quality management systems - requirements for regulatory purposes
IEC 60601 - Medical electrical equipment
IEC 62366 - Application of usability engineering to medical devices

Also see https://www.iso.org/ics/11.040.10/x/ for all ISO standards relevant to anaesthetic and respiratory equipment

##### 8.1.2
To be determined. todo

#### 8.2 Usability, user errors
The first layer of usability will be the ability to hardware teams to utilize the VentOS
software by systematically providing drivers to their project-specific hardware.
VentOS will prove this usability by working early with selected teams to accomplish this.

Usability can be separated into errors by the integrating teams and errors by the actual
medical professionals. Errors by the integrating teams will be caught by a combination
of automated user tests (in software) and actualy physcial tests performed with, for example,
test lungs. User error created by medical professionals is largely the responsibility of the
integrating teams, but VentOS hopes to make their job easier by providing extensive software
alarm conditions.


### 9. Design Plan
#### 9.1 Design Activities

##### 9.1.1 Planning
Planning shall be completed using GitLab, Slack and online video calls. All
planning documentation shall be recorded in Markdown and stored on GitLab under
version control.

##### 9.1.2 Requirements Analysis
Requirements shall be determined by discussion with open source ventilator
design teams and medical professionals.

##### 9.1.3 Conceptual Design
Concept development of software features shall be conducted on Git branches.
Experimental branches are not subject to this Quality Plan but may become a
development branch upon raising an issue as per secion 9.2. A new development
branch shall be created from the experimental branch and the feature shall
become subject to this Quality Plan. Experimental branches shall be named with
this convention: `exp_feature_name`. For example: `exp_PRVC_mode`

##### 9.1.4
Detail Design and Integration

The detailed design will be devleoped in an agile fashion. Integration
testing will be carried on by hardware teams that use the VentOS framework.
By using software techniques knowns as "mocks, fakes, and stubs" developed
by the automated testing community, we can minimize the difference between
an simulation hardware environment and a true hardware environment.
PlatformIO allows us to test code on multiple processors.

##### 9.1.5 Verification

Although VentOS will use commmunical code and design reviews, it will rely
heavily on automated test suites. The automated tests provide both verification
but also organize that verification and mange risks associated with changes
as the framework is being devleoped.

##### 9.1.6 Validation
Validation that VentOS meets the needs of target users will consist of
three parts:
.1 decrease in the burden to hardware teams by relieveing them of software
development complexity,
.1 validation that the software runs on their machines and simplifies their tasks, and
.1 perception that the VentOS project has made regulatory clearance easier by providing
ready-made documentaiton for some fraction of the application process.

##### 9.1.7 Design Transfer

As a fully open project, any team is welcome to use VentOS without contacting us,
so long as they conform to our licensing. However, we expect to work with integrating
teams specifically around hardware-specific driver development. Although driver
development is outside the responsibility of this project, the frameworks for these
drivers is in the scope of this project and represents a major design that
can be transferred and communicated to integrators.

##### 9.1.8 Control of Changes
Git is used to control all design, documentation and software changes. Each
feature shall be raised as a GitLab Issue and a developed in a separate feature
branch.

Each feature branch shall be named with this convention:
`dev_issuenumber_feature_name`. For example: `dev_74_signal_filter`.

#### 9.2 Design Issues
Design issues shalled be raised using the GitLab Issues feature. The Issue is
open for public comment on GitLab and shall be raised at the next available
Weekly Meeting for verbal discussion. Upon agreement, actionable fixes to
Design Issues shall be recorded on the GitLab Issue and Meeting Minutes. All
fixes shall be subject to section 9.3 before being merged into the Master
Branch.

#### 9.3 Design Reviews and Approvals
Design Reviews shall be held at Weekly Meetings, time permitting. Additional
Design Review Meetings may be held to complete the design review. The design
review shall include experts in the subject matter area. Upon completion of the
design review, a GitLab Merge Request shall be raised.

Design Approvals are to be completed using the GitLab Merge Request feature.
Any 2 of 3 of the following persons shall approve the GitLab merge request:
- Dr. Erich Schulz
- Robert L. Read, PhD
- Ben Coombs

### 10. Manufacturing Plan

#### 10.1 Off the shelf components
Off the shelf software is used in the development and testing
of VentOS. PlatformIO is the software used for VentOS firmware development
in C++. PlatformIO has demonstrable use in safety critical applications for
commercial products [todo ref?]. GitLab is used for source control and communication.
GitLab is used by [todo ref?]. Python is used for ancillary software development
such as data analysis; Python is not used in the VentOS firmware itself.

The VentOS team liases with ADOPTING PARTNERS to understand their general hardware
requirements, however the specific hardware design, implementation,
procurement and testing is out is outside the scope of the VentOS firmware
and is the responsibility of ADOPTING PARTNERS.

#### 10.2 Custom components manufacturing and Sub-systems assembly
VentOS embedded firmware will be custom C++.
Hardware is outside the scope of VentOS and is the responsibility of ADOPTING PARTNERS.
However, VentOS will invest heavily in simulation of hardware components to
minimize risk in integration with actual hardware.

#### 10.3 System assembly, testing, packaging and labelling
VentOS embedded firmware will be Unit Tested with PlatformIO and
Continuous Integration testing using GitLab.
Hardware integration testing and assembly, packaging and labelling shall be
the responsibility of ADOPTING PARTNERS.

#### 10.4 Critical components.
A Class C medical device contains numerous critical hardware
components. Design and development of hardware including, but not
limited to electrical components, PCBs, sensors, valves, touch screens and
chassis is outside the scope of VentOS. VentOS embedded firmware shall be used
on hardware that complies to relevant standards ISO standards and follows
medical hardware design best practices.

VentOS embedded firmware is designed and developed to ISO standards and in
co-ordination with experts and in line with fault tolerant systems best
practices. Final implementation, testing and deployment of any or all
of the VentOS embedded firmware in a medical device is the responsibility
of ADOPTING PARTNERS.

The VentOS team makes no guarantees and assumes no responsibility for the
design, development, implementation, use or service of any product that
uses all or part of the VentOS embedded firmware.

#### 10.5 Actual implementation
VentOS is implemented in C++. The version of C++ is yet to be determined (todo).

VentOS embedded firmware is open source and freely available on GitLab
for installation onto ventilator hardware by ADOPTING PARTNERS.

### 11. Product Delivery
VentOS source code is delivered from the GitLab repository.
Installation of VentOS embedded firmware, co-ordination and communication to
end-users is the responsibility of ADOPTING PARTNERS.

### 12. Maintenance and Service
VentOS source code is available from the GitLab repository. The VentOS team
will communicate updates and bug fixes to the ADOPTING PARTNERS.
Hardware integration testing and installation of firmware updates is the
responsibility of ADOPTING PARTNERS.
ADOPTING PARTNERS shall communicate any bug fixes found during testing and
deployment to the VentOS team.
