# Software Development Plan

## Requirements
According to IEC:63204 5.1.1 this plan must address:
  - a) the PROCESSES to be used in the development of the SOFTWARE SYSTEM;
  - b) the DELIVERABLES (includes documentation) of the ACTIVITIES and TASKS;
  - c) TRACEABILITY between SYSTEM requirements, software requirements, SOFTWARE SYSTEM test, and RISK CONTROL measures implemented in software;
  - d) software configuration and change management, including SOUP CONFIGURATION ITEMS and software used to support development; and
  - e) software problem resolution for handling problems detected in the SOFTWARE PRODUCTS, DELIVERABLES and ACTIVITIES at each stage of the life cycle.

Under IEC:63204 this plan must include or reference:
- "procedures for coordinating the software development and the design and development validation necessary to demonstrate the ability to provide MEDICAL DEVICE SOFTWARE that consistently meets customer requirements and applicable regulatory requirements." s4.1 and 5.1.3b
- SYSTEM requirements (5.1.3a)
- a) standards, b) methods, and c) tools associated with the development of SOFTWARE ITEMS of class C. (5.1.4)
- a plan to integrate the SOFTWARE ITEMS (including SOUP) and perform testing during integration. (5.1.5)
- details on verification (5.1.6):
  - a) DELIVERABLES requiring VERIFICATION;
  - b) the required VERIFICATION TASKS for each life cycle ACTIVITY;
  - c) milestones at which the DELIVERABLES are VERIFIED; and
  - d) the acceptance criteria for VERIFICATION of the DELIVERABLES.
- the software RISK MANAGEMENT PROCESS, including the management of RISKS relating to SOUP.
- TBC

Development will use the well established Agile manifesto, as modified to
support medical devices, aiming to meet the requirement of IEC:63204 for class
C software on an ongoing basis, with risk management in alignment with
ISO14971.

## General Plan
From each user need, specify:
- Intended use ISO14971 5.2 and reasonably foreseeable misuse
- Market Requirements (MRS)
- Product Requirements (PRD) in reference to applicable standards
- Characteristics related to safety ISO14971 5.3 (See ISO/TR 24971 for a list of questions that can serve as a guide in identifying medical device characteristics that could have an impact on safety.)
- Verification test cases (Did you design the device right?) FDA: “Verification means confirmation by examination and provision of objective evidence that specified requirements have been fulfilled.”
- Validation test cases (Did you design the right device?) FDA: “Validation means confirmation by examination and provision of objective evidence that the particular requirements for a specific intended use can be consistently fulfilled.”
- Hazard and Hazardous situations (FMEA, UFMEA)  ISO14971 5.4 (including those arising from risk control measures 7.5)
- A risk management file including:
  - Risk evaluation ISO14971 6, 7.1, 7.3 and 7.4
  - Risk control measures ISO14971 7.2 (eg coding changes, recommendations to manufacturers for device characteristics or training)
  - Certification that all risks have been considered Risk review and documentation ISO14971 7.6
  - Evaluation of residual risk 8.8
  - Identification of the person and organisation carrying out the risk analysis
  - A plan to assist manufacturers with production and post-production activities

Note: "There are ways you can accomplish design verification and design validation with the same activities, if planned properly." (ref)

Eg:

![Example Hazards](images/example_hazards.jpg)

## System Architecture Planning
General requirement brainstorming
- Define subsystems
- Create subsystem architecture and depicting it as a flow chart that shows the links between the subsystems.
Prioritize subsystems. ie for which subsystem will requirements be defined: First? Second? Third?
- Develop subsystem-level requirements for the device concepts (performed by the engineering teams) and concurrently developing system-level requirements (done by the systems engineer) with an eye to subsystem integration.
- Integrate the “locked down” or successful subsystems: Once subsystems A and B, for instance, are complete and functional, development of a prototype of an integrated subsystem AB can be initiated. As integration occurs, product development moves from meeting requirements to creating actual specifications.

## Risk Analysis
From ISO14971:

![Risk Analysis Flow](images/risk_mgmt_flow.jpg)
![Hazard Analysis Flow](images/hazard_analysis_flow.jpg)

## Agile Manifesto

From Ultimate Guide to Agile Design and Development for Medical Devices:
- The highest priority is to satisfy our customer through early and continuous delivery of work product.
- Welcome changing requirements, even late in development. Agile processes harness change for the customer's competitive advantage.
- Deliver working prototypes frequently, from a couple of weeks to a couple of months, with a preference to shorter timescales.
- Business people and developers must work together daily throughout the project.
- Build projects around motivated individuals. Give them the environment and support they need, and trust them to get the job done.
- The most efficient and effective method of conveying information to and within a development team is face-to-face conversation.
- Working prototypes are the primary measure of progress.
- Agile processes promote sustainable development. The sponsors, designers, developers, and users should be able to maintain a constant pace indefinitely.
- Continuous attention to technical excellence and good design enhances agility.
- Simplicity–the art of maximizing the amount of work not done–is essential.
- The best architectures, requirements, and designs emerge from self-organizing teams.
- At regular intervals, the team reflects on how to become more effective, then tunes and adjusts its behavior accordingly.


![Agile Flow](images/agile_flow.jpg)


## Acronyms
- MRS/MRD - Market Requirements Specification/Document: the story of the product, its primary use cases and the environment where the device will be used
- PRD - Product Requirements Document The PRD is more of a technical understanding of what will be needed in order to achieve the use cases found in the MRD.
- HA - hazard analysis
- FMEA - failure modes and effects analysis
- UFMEA - user failure modes and effects analysis
- SOUP - software of unknown providence


## Sources
What is FMEA and how is it different from Hazard Analysis?
